'use strict'

const firebase = require("firebase")
const cloudinary = require('cloudinary')
const req = require('request')
var rp = require('request-promise')
const cheerio = require('cheerio')

class ScrapSoccerController {

	async getClubs ({ request, response }) {
		var config = {
		    apiKey: "AIzaSyCghQpMNYTVcL7fXOsGf8Yf8_yJ_DMODTI",
		    authDomain: "bolameter-1e1a6.firebaseapp.com",
		    databaseURL: "https://bolameter-1e1a6.firebaseio.com",
		    projectId: "bolameter-1e1a6",
		    storageBucket: "bolameter-1e1a6.appspot.com",
		    messagingSenderId: "997107702811"
		};
		firebase.initializeApp(config);

		cloudinary.config({ 
		  cloud_name: 'dxyvnof5c', 
		  api_key: '327483874575559', 
		  api_secret: 'izvU0dNFf4IK0_zn4wxursP98wc' 
		})

		let arrclub = []
		let $ = null
		const league_id = 28
		const url = 'https://en.soccerwiki.org/league.php?leagueid=' + league_id
		await rp(url).then(function (body) {
		  $ = cheerio.load(body)
		}).then(function(e) {
			const table = $('table[cellspacing=3]').first()
			table.find('tbody tr').each((i, value) => {
				arrclub.push({
					id: parseFloat($(value).find('td').eq(1).find('a').attr('href').replace('squad.php?clubid=','')),
					name: $(value).find('td').eq(1).find('a').text()
				})
			})
		})

		for (let i = 0; i < arrclub.length; i++) {
			await rp('https://en.soccerwiki.org/squad.php?clubid=' + arrclub[i].id).then(function (body) {
			  	$ = cheerio.load(body)
			}).then(function(e) {
				const table = $('table.tabledata').first()
				arrclub[i].league_id = league_id
				arrclub[i].img = 'http://smimgs.com/images/logos/clubs/' + arrclub[i].id + '.png'
				table.find('tbody tr').each((k, value) => {
					if (k == 0) arrclub[i].manager = $(value).find('td').find('a').text()
					if (k == 1) arrclub[i].nickname = $(value).find('td').text()
					if (k == 4) arrclub[i].year_founded = !isNaN($(value).find('td').text()) ? parseFloat($(value).find('td').text()) : null
					if (k == 5) arrclub[i].stadium = $(value).find('td').find('a').text()
					if (k == 7) arrclub[i].location = $(value).find('td').text()
				})

				cloudinary.v2.uploader.upload(arrclub[i].img, 
		        {
		        	folder: 'clubs',
		        	use_filename: true,
		        	overwrite: true,
		        	unique_filename: false
		        }, 
		        function(error, result) { console.log(result, error) })

		        firebase.database().ref('clubs/' + arrclub[i].id).set(arrclub[i])
			})
		}
		response.status(200).json(arrclub)
	}

	async getPlayers ({ request, response }) {
		var config = {
		    apiKey: "AIzaSyCghQpMNYTVcL7fXOsGf8Yf8_yJ_DMODTI",
		    authDomain: "bolameter-1e1a6.firebaseapp.com",
		    databaseURL: "https://bolameter-1e1a6.firebaseio.com",
		    projectId: "bolameter-1e1a6",
		    storageBucket: "bolameter-1e1a6.appspot.com",
		    messagingSenderId: "997107702811"
		};
		if (!firebase.apps.length) {
		   firebase.initializeApp(config);
		}

		cloudinary.config({ 
		  cloud_name: 'dxyvnof5c', 
		  api_key: '327483874575559', 
		  api_secret: 'izvU0dNFf4IK0_zn4wxursP98wc' 
		})

		let arrclub = []
		let arrplayer = []

		const clubs = firebase.database().ref('clubs')
		await Promise.all(clubs.on("value", (snapshot) => {
			snapshot.forEach(function(childSnapshot) {
				const club = childSnapshot.val()
				arrclub.push(club)
			})
		}))
	}
}

module.exports = ScrapSoccerController
